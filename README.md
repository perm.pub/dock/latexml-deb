This repository automatically builds and deploys an OCI container image with LaTeXML 
whenever a Git tag starting with `image/` is pushed to this Git repository.

The OCI container registry can be browsed at
<https://gitlab.com/perm.pub/dock/latexml-deb/container_registry>.

Example usage:

```
podman run -it --rm registry.gitlab.com/perm.pub/dock/latexml-deb latexml --version
```

For repeated calls, it can be convenient to define a Bash function as follows:

```
latexml() {
  podman run --rm -v=.:/mnt -w=/mnt registry.gitlab.com/perm.pub/dock/latexml-deb latexml "$@"
}
```

Then, you can run LaTeXML in the container as if it were installed locally like this:
```
latexml --destination=xml/main.xml main.tex
```

However, keep in mind that LaTeXML inside the container does not have access to 
directories outside the current working directory.

To install Podman, visit [podman.io/get-started](https://podman.io/get-started).
All the basic subcommands of `docker` are also supported by `podman`.
When running from Linux, Podman, which does not run as a daemon,
has fewer security issues than Docker, which does run as a daemon.

Mac users may need to run `podman machine init --volume $HOME:$HOME` before `podman machine start`.
On Mac, there is an intermediary virtual machine (VM) between your Mac and the container,
and the VM needs access to your work directory.
